# File name: blogPostReader.py
#
# Description: A class to parse the Markdown file for a blog post
# and store and return data as needed to the rest of the app
#
# Author: Zac Belado - pixelgeek@gmail.com
# Project: Victor
# 
# Date: Tuesday April 04 2023

import datetime
from pathlib import Path
from typing import Union

import frontmatter

from rich.text import Text
from rich.table import Table


class blogPostReader:

    def __init__(self) -> None:
        self.currentPost = {
            "path": Path,
            "title": "",
            "postDate": None,
            "postDisplayDate": "",
            "description": "",
            "summary": "",
            "filename": "",
            "tags": "",
            "categories": "",
            "frontmatter": {},
            "post": "",
            "draft": False,
            "fileClean": True
        }
        self._displayDateFormat = ""
        self._postState = None
        self.colors = {}

    def getPost(self) -> str:
        return self.currentPost["post"]

    def formatKey(self, keyName: str) -> Text:
        # take the key name and do some formatting on it based on the key
        # most of them will not need to be formatted

        match keyName:

            case "draft":
                draftColor = "green3"
                if self.currentPost["draft"]:
                    draftColor = "red3"
                return Text(str(self.currentPost["draft"]), style=draftColor)

            case other:
                keyValue = self.currentPost[keyName]
                textStyle = "none"

                # if a value has the word 'No' at the beginning then disable the text
                if keyValue[0:2] == "No":
                    textStyle = "grey66 italic"

                return Text(keyValue, style=textStyle)

    def getPostDisplayText(self) -> Table:
        # create a Rich Table object to send back to the app
        tableTitle = Text("Content Details", style="Bold black")

        dataTable = Table(title=tableTitle, border_style="none")
        dataTable.add_column("Key", justify="right", no_wrap=True)
        dataTable.add_column("Value", justify="left", no_wrap=False)

        # create a list of lists and iterate over it to build the rows
        rowKeys = [
            ["Title", "title"],
            ["Date", "postDate"],
            ["Draft", "draft"],
            ["Filename", "filename"],
            ["Tags", "tags"],
            ["Categories", "categories"],
            ["Summary", "summary"],
            ["Description", "description"],
        ]

        for aRow in rowKeys:
            dataTable.add_row(Text(aRow[0], style="bold"), self.formatKey(aRow[1]))

        # and return the table to get it displayed
        return dataTable

    def loadBlogPost(self, dateFormat: str, filePath: Union[str, Path], colorData: dict) -> None:

        # store the date format in case we need it later
        self._displayDateFormat = dateFormat

        # this is a hack to use the defined Textual colours in Rich
        self.colors = colorData

        # check the type of the path sent
        # convert it if it is a string
        if type(filePath) is str:
            filePath = Path(filePath)

        # test the filePath and then use frontmatter to load the file
        if filePath.is_file:
            with open(filePath) as markdownFile:
                metadata, content = frontmatter.parse(markdownFile.read())

            # now get the required data for the currentPost
            self.currentPost["frontmatter"] = metadata
            postKeys = metadata

            self.currentPost["title"] = postKeys["title"]

            # some files don't have a date in their frontmatter
            postDate = postKeys.get("date", "No date")
            if type(postDate) is datetime.datetime:
                self.currentPost["postDate"] = postDate.strftime(self._displayDateFormat)
            else:
                self.currentPost["postDate"] = "No date"

            self.currentPost["draft"] = postKeys["draft"]

            # tags and categories are either a list or a str
            self.currentPost["tags"] = postKeys.get("tags", "No tags")
            if type(self.currentPost["tags"]) is list:
                self.currentPost["tags"] = ", ".join(self.currentPost["tags"])

            self.currentPost["categories"] = postKeys.get("categories", "No categories")
            if type(self.currentPost["categories"]) is list:
                self.currentPost["categories"] = ", ".join(self.currentPost["categories"])

            self.currentPost["filename"] = filePath.name
            self.currentPost["description"] = postKeys.get("description", "No description")
            self.currentPost["summary"] = postKeys.get("summary", "No summary")
