#
# DirectoryTree class for Victor
# author: Zac Belado pixelgeek@gmail.com
#
# Will subclass the existing DirectoryTree so
# that I can get it to filter files. Will not need this in the future
#

from typing import Iterable

from dataclasses import dataclass
from pathlib import Path

from textual.widgets import DirectoryTree
from textual.widgets.tree import TreeNode


@dataclass
class DirEntry:
    # Attaches directory information to a node.
    path: str
    is_dir: bool
    loaded: bool = False


class victorDirectoryTree(DirectoryTree):
    # subclass the load_directory method. This is where the
    # DirectoryTree shows each node in the tree. This new
    # method will remove everything but Markdown files

    def load_directory(self, node: TreeNode[DirEntry]) -> None:
        assert node.data is not None
        dir_path = Path(node.data.path)

        node.data.loaded = True
        directory = sorted(
            list(dir_path.iterdir()),
            key=lambda path: (not path.is_dir(), path.name.lower()),
        )
        for path in directory:
            addNode = False

            # add the node if it is a directory or if it is an .md file
            if path.is_dir():
                # some apps have directories starting with a .
                if path.name[0] != '.':
                    addNode = True
            else:
                if path.suffix == ".md":
                    addNode = True

            if addNode:
                node.add(
                    path.name,
                    data=DirEntry(str(path), path.is_dir()),
                    allow_expand=path.is_dir(),
                )
        node.expand()
