#
# File: logo.py
# Author: Zac Belado
#
# Created: April 2, 2023
#
# Description: A Textual class for the logo of the Victor CMS
#

from textual.widgets import Label
from textual.app import ComposeResult

logoData = """+-+-+-+-+-+-+
|V|i|c|t|o|r|
+-+-+-+-+-+-+
"""


class LogoArea(Label):
    # A class to, currently, hold the ASCII text for the logo
    # but I might add some weirdness later

    def compose(self) -> ComposeResult:
        # create a space for the ASCII logo
        yield Label(logoData, id="logoText")
