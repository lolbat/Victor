#
# Config class for the Victor - Hugo CMS
# author: Zac Belado pixelgeek@gmail.com
#
# Reads and stores the configuration data from victor.toml
# Will have more additions in the future
#

import tomllib
from shutil import copyfile
from pathlib import Path

from platformdirs import PlatformDirs

from textual import log

class LoadConfig:
    # Class to load the TOML config file
    # and to also set and read values
    
    contentPath = Path()
    editorPath = Path()
    configPath = Path()
    dateFormat = ""
    
    def __init__(self) -> None:
        
        # use platformdirs to get the path to the approipriate user pref directory
        appname = "Victor"
        appauthor = "Zac"
        
        self.userDirs = PlatformDirs(appname, appauthor)
        self.configPath = Path(self.userDirs.user_data_dir) / "victor.toml"
        
        # Try to load the config file
        self.loadConfigData()

    def loadConfigData(self) -> None:
        
        # is there a config file?
        if not self.configPath.exists():
            log("Config file not found")
            
            # first make the directory
            try:
                Path(self.userDirs.user_data_dir).mkdir(parents=True, exist_ok=True)
                
                try: 
                    # First run or some other issue so create a blank config
                    localFilePath = Path.cwd() / "default.toml"
                    copyfile(str(localFilePath), str(self.configPath))
                    
                except OSError as error:
                    log("Unable to copy config file: {error.args[0]} {error.args[1]}")
                    
            except FileNotFoundError:
                log(f"Unable to create path {self.userDirs.user_data_dir}")
                
            except FileExistsError:
                log (f"Directory already exists {self.userDirs.user_data_dir}")
                
            except OSError as error:
                log(f"Error creating config file {error.args[0]} {error.args[1]}")
  
        # Try to read the config data
        try:
            with open(self.configPath, mode="rb") as configFile:
                configData = tomllib.load(configFile)

        except OSError as error:
            # No config file so copy the default to the data directory
            log(f"Error opening config file {error.args[0]} {error.args[1]}")
            log(self.configPath)
            log(self.userDirs.user_data_dir)

        else:
            self.contentPath = Path(configData["paths"]["root_path"]) / "content"
            self.editorPath = Path(configData["editor"]["editor_path"])
            self.dateFormat = configData["display"]["date_format"]
