#
# screenDisplay.py
# Description: A Textual app to display an external screen
# while it is being developed
# 
# Created: Tue Apr 25 2023
# Author: Zac Belado pixelgeek@gmail.com
#

import logging

from textual.app import App, ComposeResult
from textual.containers import Container
from textual.widgets import TextLog, Header, Footer
from textual import log
from textual.logging import TextualHandler

from screens.config_screen import configScreen

logging.basicConfig(
    level="DEBUG",
    handlers=[TextualHandler()],
)


class screenDisplay(App):

    BINDINGS = [("q", "quit", "Quit")]
    CSS_PATH = "./css/screenDisplay.css"

    def compose(self) -> ComposeResult:
        yield Header()
        with Container():
            yield TextLog(highlight=True, markup=True)
        yield Footer()

    def on_mount(self) -> None:
        self.install_screen(configScreen(), name="config")
        self.push_screen("config")

        log("I opened a new screen")


if __name__ == "__main__":
    screenDisplay().run()    
