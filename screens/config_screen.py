#
# The configuration screeen for Victor
# author: Zac Belado pixelgeek@gmail.com
#
# Displays and saves the user entered configuration data
# for the app

from textual.app import ComposeResult
from textual.containers import Vertical
from textual.screen import Screen
from textual.widgets import Footer

from classes.formfield import formField


class configScreen(Screen):
    
    BINDINGS = [("s", "saveConfig", "Save")]
    CSS_PATH = "config.css"

    def compose(self) -> ComposeResult:
        
        # build the layout for the configuration screen
        
        with Vertical(id="formLeft"):
            yield formField("Hugo Path", "The path to your Hugo install")
            yield formField("Editor Path", "The path to your Markdown editor")
            yield formField("Date Format", "", "", "%A %B %d, %Y")
        
        with Vertical(id="formRight"):
             ...
             
        yield Footer()
        