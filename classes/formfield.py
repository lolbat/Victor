#
# formfield.py 
# Description: A Textual class to create a three widget
# form field to use in Victor
#
# Created: Wed Apr 26 2023
# Author: Zac Belado pixelgeek@gmail.com
#

from typing import NamedTuple

from textual.app import ComposeResult
from textual.widgets import Label, Input, Static

class formData(NamedTuple):
    label: str
    note: str
    data: str


class formField(Static):

    def __init__(
        self, 
        labelText: str,
        noteText: str = "",
        fieldText: str = "",
        defaultText: str = "",
    ) -> None:

        # check to see if the field text is blank and if so then use the
        # defaultText

        if fieldText == "":
            fieldText = defaultText

        self.data = formData(labelText, noteText, fieldText)

        super().__init__()

    def compose(self) -> ComposeResult:
        
        yield Label(self.data.label, id="formLabel")
        yield Label(self.data.note, id="formNote")
        yield Input(self.data.data, id="formData")
        
        return super().compose()
    