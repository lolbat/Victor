#
# Victor - A simple CMS for the Hugo SSG
#
# based on the code browser example file
# from Textual
#
# Author: Zac Belado pixelgeek@gmail.com
# version 0.1.0
#

from pathlib import Path

from textual.app import App, ComposeResult
from textual.containers import Container
from textual.widgets import DirectoryTree, Footer, Header, Static, TextLog

from widgets.config import LoadConfig
from widgets.logo import LogoArea
from widgets.directoryTree import victorDirectoryTree
from widgets.blogPostReader import blogPostReader

config = LoadConfig()
blogReader = blogPostReader()


def buildMDPath(mdName: Path) -> Path:
    # return a path to the MD file based on the content directory setting
    return config.contentPath / mdName


class Victor(App):
    # Textual code browser app.

    CSS_PATH = "./css/victor.css"
    BINDINGS = [("q", "quit", "Quit"), ("c", "configure", "Config")]

    def compose(self) -> ComposeResult:
        # Compose the UI.
        self.dark = False

        # set the DirectoryTree path to the one stored in the config file
        path = config.contentPath

        yield Header()
        with Container(id="topBar"):
            yield LogoArea(id="logo")
            yield Static(id="controls")
        with Container(id="bottomBar"):
            yield victorDirectoryTree(path, id="tree-view")
            yield TextLog(id="postDetails")
        yield Footer()

        # make the title Victor no matter what the class name ends up being
        self.title = "Victor"

    def on_mount(self) -> None:
        # set some values for the Directory Tree

        dtree = self.query_one(victorDirectoryTree)
        dtree.focus()
        dtree.show_root = False
        dtree.show_guides = False

    # Called when the user click a file in the directory tree.
    async def on_directory_tree_file_selected(
            self, event: DirectoryTree.FileSelected
    ) -> None:
        # get the text, text-dimmed and text-disabled colours
        colourList = self.stylesheet._variables
        colours = {
            "text": colourList['text'],
            "text-muted": colourList['text-muted'],
            "text-disabled": colourList['text-disabled']
        }

        # send filepath to blogPostReader
        # then get the display data for the post
        blogReader.loadBlogPost(config.dateFormat, event.path, colours)
        displayText = blogReader.getPostDisplayText()

        # print it in the TextLog
        postDisplay = self.query_one(TextLog)
        postDisplay.clear()
        postDisplay.write(displayText, expand=True)

        # try to add a button to the TextLog


if __name__ == "__main__":
    Victor().run()
